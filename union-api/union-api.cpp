﻿
#include <iostream>
#include "Union/Array.h"
#include "Union/VDFS.h"
#include "Union/Hook.h"
#include "Union/Stream.h"
#include "Union/String.h"
#include "Union/Thread.h"
#include "Union/Updater.h"
#include "Union/Dll.h"

int main()
{
  Union::StringANSI( "Hello, world!" ).StdPrintLine();
}